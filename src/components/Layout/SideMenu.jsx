import React, { useState } from "react";
import { Layout, Menu } from "antd";

import { Link, useLocation } from "react-router-dom";
import { userRouter } from "../../routes/userRouter";

export default function SideMenu() {
  const { Sider } = Layout;
  const [collapsed, setCollapsed] = useState(false);
  const location = useLocation();

  return (
    <Sider
      breakpoint="lg"
      collapsedWidth="0"
      onBreakpoint={(broken) => {}}
      collapsible
      collapsed={collapsed}
      onCollapse={(value) => setCollapsed(value)}
    >
      <div className="logo flex justify-center items-center">
        <p className="text-xl text-white my-auto py-2">Dashboard</p>
      </div>
      <Menu
        theme="dark"
        defaultSelectedKeys={[location.pathname]}
        mode="inline"
      >
        {userRouter.map((e, i) => {
          return (
            e.isLayout && (
              <Menu.Item key={e.path} className="items-center">
                {e.icon}
                <span>{e.title}</span>
                <Link to={e.path}></Link>
              </Menu.Item>
            )
          );
        })}
      </Menu>
    </Sider>
  );
}
