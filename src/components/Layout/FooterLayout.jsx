import { Footer } from "antd/lib/layout/layout";
import React from "react";

export default function FooterLayout() {
  return (
    <Footer
      style={{
        textAlign: "center",
      }}
    >
      Ant Design ©2018 Created by Ant UED
    </Footer>
  );
}
