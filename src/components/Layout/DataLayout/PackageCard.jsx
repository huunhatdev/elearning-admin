import { Card } from "antd";
import React from "react";
import { toVND } from "../../Utils/Vnd";

export default function PackageCard({ pack }) {
  return (
    <Card
      title={<h2 className="text-2xl text-red-500 m-0">{pack.title} </h2>}
      bordered
      hoverable
    >
      <div className="text-left space-y-2">
        <div>
          <span>Soạn: </span>
          <span className=" font-medium text-md text-blue-500">
            {pack.sms.toUpperCase()}
          </span>
        </div>
        <p>Giá gói cước: {toVND(pack.price)}</p>
        <p className="">Hoa hồng: {toVND(pack.rose)}</p>
      </div>
    </Card>
  );
}
