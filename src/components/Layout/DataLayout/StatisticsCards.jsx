import React from "react";

export default function StatisticsCards({ lable, icon, value }) {
  return (
    <div className="bg-gray-800 shadow-lg rounded-md flex items-center justify-between p-3 border-b-4 border-gray-600 text-white font-medium group w-full">
      <div className="flex justify-center items-center w-14 h-14 bg-white rounded-full transition-all duration-300 transform group-hover:rotate-12 text-slate-700 text-3xl">
        {icon}
      </div>
      <div className="text-right">
        <p className="text-3xl mb-2">{value}</p>
        <p className="mb-0">{lable}</p>
      </div>
    </div>
  );
}
