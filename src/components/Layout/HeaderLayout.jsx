import { DownOutlined } from "@ant-design/icons";
import { Avatar, Button, Image, Popover } from "antd";
import { Header } from "antd/lib/layout/layout";
import React from "react";
import { useNavigate } from "react-router";
import { localStorageService } from "../../services/localStorageService";

export default function HeaderLayout() {
  let navigate = useNavigate();
  const handleLogout = () => {
    localStorageService.removeUserInfo();
    window.location.reload();
    // setTimeout(() => {
    //   navigate("/login");
    // }, 1500);
  };
  let info = localStorageService.getUserInfo();
  console.log("info: ", info);
  return (
    <div className="flex justify-end items-center h-12 bg-slate-700 shadow-md pd-2 pr-5 space-x-4 text-white">
      <div>Xin chào, {info.hoTen}</div>
      <Avatar src="https://joeschmoe.io/api/v1/random" />
      <Popover
        placement="bottomRight"
        title={"Thông tin"}
        content={
          <div className="space-y-1">
            <p>Cập nhật thông tin</p>
            <p
              onClick={() => {
                handleLogout();
              }}
              className=" cursor-pointer"
            >
              Đăng xuất
            </p>
          </div>
        }
        trigger="click"
      >
        <DownOutlined />
      </Popover>
    </div>
  );
}
