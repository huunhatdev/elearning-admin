import { UploadOutlined } from "@ant-design/icons";
import { Button, Form, Input, message, Select, Upload } from "antd";
import React, { useEffect, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { userService } from "../../services/userService";
const { Option } = Select;

export default function LessonAddEdit({ type }) {
  const [typePage, setTypePage] = useState({
    title: null,
    button: null,
    info: null,
  });
  const [category, setCategory] = useState([]);
  const [GVList, setGVList] = useState([]);
  const [defaultFile, setDefaultFile] = useState();
  const { course } = useParams();
  const navigate = useNavigate();
  const upLoadFile = (options) => {
    const { onSuccess, onError, file, onProgress } = options;
    setDefaultFile(file);
    onSuccess(file);
  };
  const onFinish = (values) => {
    let form_data = new FormData();
    for (let key in values) {
      form_data.append(key, values[key]);
    }
    form_data.append("maNhom", "GP01");
    form_data.append("hinhAnh", defaultFile);
    console.log(defaultFile);
    switch (type) {
      case "edit":
        userService
          .postUpdateCourse(form_data)
          .then((res) => {
            console.log(res);
            message.success("Cập nhật thông tin thành công");
            setTimeout(() => {
              navigate("/danh-sach-khoa-hoc");
            }, 2000);
          })
          .catch((err) => {
            message.error(err.response.data);
          });
        break;
      case "add":
        userService
          .postAddCourse(form_data)
          .then((res) => {
            console.log(res);
            message.success("Đăng kí tài khoản thành công");
            setTimeout(() => {
              navigate("/danh-sach-khoa-hoc");
            }, 2000);
          })
          .catch((err) => {
            message.error(err.response.data);
          });
    }
  };

  const handleOnChangeImg = ({ file, fileList, event }) => {};

  useEffect(() => {
    userService
      .getCategoryCourse()
      .then((res) => {
        setCategory(res.data);
      })
      .catch((err) => {
        message.error(err.response.data);
      });
    userService
      .getDataUser()
      .then((res) => {
        let gvArr = res.data.filter((e) => e.maLoaiNguoiDung === "GV");
        setGVList(gvArr);
      })
      .catch((err) => {
        console.log(err);
      });
    switch (type) {
      case "add":
        setTypePage({
          title: "Thêm khoá học",
          button: "Thêm",
          info: null,
        });
        break;
      case "edit":
        userService
          .getInfoCourse(course)
          .then((res) => {
            setTypePage({
              title: "Chỉnh sửa khoá học",
              button: "Cập nhật",
              info: res.data,
            });
          })
          .catch((err) => {});
        break;
    }
  }, []);

  return (
    typePage.title !== null && (
      <div className="p-5">
        <h2>{typePage.title}</h2>
        <div>
          <Form
            name="user"
            onFinish={onFinish}
            autoComplete="off"
            layout="vertical"
            initialValues={{
              maKhoaHoc: typePage.info?.maKhoaHoc,
              biDanh: typePage.info?.biDanh,
              tenKhoaHoc: typePage.info?.tenKhoaHoc,
              luotXem: typePage.info?.luotXem,
              danhGia: typePage.info?.danhGia,
              maLoaiNguoiDung: typePage.info?.maLoaiNguoiDung,
            }}
          >
            <div className="grid lg:grid-cols-2 lg:gap-5">
              <div>
                <Form.Item
                  label="maKhoaHoc"
                  name="maKhoaHoc"
                  rules={[
                    { required: true, message: "Please input your username!" },
                  ]}
                >
                  <Input disabled={type === "edit" ? true : false} />
                </Form.Item>

                <Form.Item
                  label="Bí danh"
                  name="biDanh"
                  rules={[
                    { required: true, message: "Please input your password!" },
                  ]}
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  label="Tên khoá học"
                  name="tenKhoaHoc"
                  rules={[
                    {
                      required: true,
                      message: "Please input your name course!",
                    },
                  ]}
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  initialValue={typePage.info?.danhMucKhoaHoc.maDanhMucKhoahoc}
                  label="Mã danh mục khoá học"
                  name="maDanhMucKhoaHoc"
                  rules={[
                    {
                      required: true,
                      message: "Please input your name course!",
                    },
                  ]}
                >
                  <Select
                    defaultValue={
                      typePage.info?.danhMucKhoaHoc.maDanhMucKhoahoc
                    }
                  >
                    {category.map((e, i) => (
                      <Option key={i} value={e.maDanhMuc}>
                        {e.tenDanhMuc}
                      </Option>
                    ))}
                  </Select>
                </Form.Item>
              </div>
              <div>
                <Form.Item
                  label="Lượt xem"
                  name="luotXem"
                  rules={[
                    { required: true, message: "Please input your username!" },
                  ]}
                >
                  <Input />
                </Form.Item>

                <Form.Item
                  initialValue={"0"}
                  label="Đánh giá"
                  name="danhGia"
                  rules={[{ message: "Please input your password!" }]}
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  initialValue={typePage.info?.nguoiTao.taiKhoan}
                  label="Người tạo"
                  name="taiKhoanNguoiTao"
                  rules={[
                    {
                      // required: true,
                      message: "Please input your password!",
                    },
                  ]}
                >
                  <Select defaultValue={typePage.info?.nguoiTao.taiKhoan}>
                    {GVList.map((e, i) => (
                      <Option key={i} value={e.taiKhoan}>
                        {e.hoTen}
                      </Option>
                    ))}
                  </Select>
                </Form.Item>

                <Upload
                  maxCount={1}
                  onChange={handleOnChangeImg}
                  customRequest={upLoadFile}
                  accept="image/jpg"
                >
                  <Button icon={<UploadOutlined />}>Upload</Button>
                </Upload>
              </div>
            </div>
            <div className="flex justify-between">
              <Link to="/danh-sach-khoa-hoc">{"<< Quay lại"} </Link>
              <Button type="primary" htmlType="submit">
                {typePage.button}
              </Button>
            </div>
          </Form>
        </div>
      </div>
    )
  );
}
