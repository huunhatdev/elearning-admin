import { Button, Modal } from "antd";
import React, { useCallback, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { courseChange } from "../../../redux/Slices/changeSlice";
import ModalCourse from "./ModalCourse";
import ModalUserAttended from "./ModalUserAttended";
import ModalUserWaitConfirm from "./ModalUserWaitConfirm";

export default function ModalCourseMain({
  handleVisible,
  openModal,
  maKhoaHoc,
}) {
  const handleCancel = () => {
    handleVisible(false);
  };
  let handleRender = useSelector((state) => state.changeSlice.course);
  const dispatch = useDispatch();
  const handleChangeCourse = useCallback(() => {
    console.log(1);
    dispatch(courseChange());
  });
  useEffect(() => {
    if (openModal) {
      handleChangeCourse();
    }
  }, [openModal]);
  return (
    <>
      <Modal
        title="Ghi danh khoá học"
        visible={openModal}
        onCancel={handleCancel}
        width={1000}
        footer={[
          <Button key="back" onClick={handleCancel} danger>
            Đóng
          </Button>,
        ]}
      >
        <ModalCourse
          maKhoaHoc={maKhoaHoc}
          handleRender={handleRender}
          handleChangeCourse={handleChangeCourse}
        />
        <ModalUserWaitConfirm
          maKhoaHoc={maKhoaHoc}
          handleRender={handleRender}
          handleChangeCourse={handleChangeCourse}
        />
        <ModalUserAttended
          maKhoaHoc={maKhoaHoc}
          handleRender={handleRender}
          handleChangeCourse={handleChangeCourse}
        />
      </Modal>
    </>
  );
}
