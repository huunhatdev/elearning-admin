import { message, Table } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { courseChange } from "../../../redux/Slices/changeSlice";
import { userService } from "../../../services/userService";
import { tableUserWaitConfirm } from "../../../utils/contentTable/tableUserWaitConfirm";

export default function ModalUserWaitConfirm({
  maKhoaHoc,
  handleRender,
  handleChangeCourse,
}) {
  const [data, setData] = useState([]);
  useEffect(() => {
    userService
      .getUserWaitConfirm(maKhoaHoc)
      .then((res) => {
        let temp = res.data.map((e, i) => {
          return {
            ...e,
            i: i + 1,
            actions: {
              onConfirm: () => {
                userService
                  .postConfirmCourse({ ...e, maKhoaHoc: maKhoaHoc })
                  .then((res) => {
                    message.success(res.data);
                    handleChangeCourse();
                  })
                  .catch((err) => {
                    console.log(err.response.data);
                  });
              },
              onCancel: () => {
                userService
                  .postCancelCourse({ ...e, maKhoaHoc: maKhoaHoc })
                  .then((res) => {
                    message.success(res.data);
                    handleChangeCourse();
                  })
                  .catch((err) => {
                    console.log(err.response.data);
                  });
              },
            },
          };
        });
        console.log(temp);
        setData(temp);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [handleRender]);

  return (
    data.length > 0 && (
      <div className="p-2">
        <h2 className="py-2">Danh sách học viên chờ xác nhận</h2>
        <Table size="small" dataSource={data} columns={tableUserWaitConfirm} />
      </div>
    )
  );
}
