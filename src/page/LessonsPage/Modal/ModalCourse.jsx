import { Button, Form, message, Select } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { courseChange } from "../../../redux/Slices/changeSlice";
import { userService } from "../../../services/userService";
const { Option } = Select;

export default function ModalCourse({
  maKhoaHoc,
  handleChangeCourse,
  handleRender,
}) {
  const [userList, setUserList] = useState([]);
  const handlRegister = (data) => {
    userService
      .postRegisterCourse({ ...data, maKhoaHoc: maKhoaHoc })
      .then((res) => {
        message.success(res.data);
        handleChangeCourse();
      })
      .catch((err) => {
        message.error(err.response.data);
      });
  };

  useEffect(() => {
    userService
      .getUserUnregister(maKhoaHoc)
      .then((res) => {
        setUserList(res.data);
      })
      .catch((err) => {});
  }, [handleRender]);
  return (
    <div className="p-2 border-b border-b-slate-200">
      <Form name="registerCourse" onFinish={handlRegister} autoComplete="off">
        <div className="flex">
          <div className="w-10/12">
            <Form.Item
              label="Tên học viên"
              name="taiKhoan"
              rules={[{ required: true, message: "Please chose your course!" }]}
            >
              <Select
                style={{ width: "100%" }}
                showSearch
                placeholder="Select a course"
                optionFilterProp="children"
                filterOption={(input, option) =>
                  option.children.toLowerCase().includes(input.toLowerCase())
                }
              >
                {userList.map((e, i) => (
                  <Option key={i} value={e.taiKhoan}>
                    {e.hoTen}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </div>
          <div className="w-2/12 text-right">
            <Button type="primary" htmlType="submit">
              Ghi danh
            </Button>
          </div>
        </div>
      </Form>
    </div>
  );
}
