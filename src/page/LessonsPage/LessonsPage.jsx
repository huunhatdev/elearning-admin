import { Button, message, Table } from "antd";
import React, { useCallback, useEffect, useState } from "react";
import { Input } from "antd";
import { userService } from "../../services/userService";
import { Link, useNavigate } from "react-router-dom";
import { tableCourse } from "../../utils/contentTable/tableCourse";
import ModalCourseMain from "./Modal/ModalCourseMain";

const { Search } = Input;

export default function LessonsPage() {
  const [dataCourse, setDataCourse] = useState([]);
  const [openModal, setOpenModal] = useState(false);
  const [maKhoaHoc, setMaKhoaHoc] = useState(null);
  const navigate = useNavigate();
  const handleVisible = useCallback((e) => {
    setOpenModal(e);
  });
  const fetch = () => {
    userService
      .getListCourse()
      .then((res) => {
        let dataClone = res.data.map((e, i) => {
          return {
            ...e,
            i: i + 1,
            actions: {
              onRegister: () => {
                handleVisible(true);
                setMaKhoaHoc(e.maKhoaHoc);
              },
              onEdit: () => {
                navigate(`chinh-sua/${e.maKhoaHoc}`);
              },
              onDelete: () => {
                userService
                  .delCourse(e.maKhoaHoc)
                  .then((res) => {
                    message.success(res.data);
                    fetch();
                  })
                  .catch((err) => {
                    message.error(err.response.data);
                  });
              },
            },
          };
        });
        setDataCourse(dataClone);
      })
      .catch((err) => {});
  };
  useEffect(() => {
    fetch();
  }, []);

  const onSearch = (value) => fetch(value);
  const onChange = (e) => {
    if (e.target.value === "") {
      fetch(null);
    }
  };
  return (
    <div className="p-5 space-y-5">
      {
        <ModalCourseMain
          handleVisible={handleVisible}
          openModal={openModal}
          maKhoaHoc={maKhoaHoc}
        />
      }
      <div className="text-right  pb-0">
        <Link to="them-moi">
          <Button type="primary"> + Thêm khoá học</Button>
        </Link>
      </div>
      <div className="">
        <Search
          placeholder="Nhập tên hoặc tài khoản người dùng"
          onSearch={onSearch}
          onChange={(e) => onChange(e)}
          enterButton
        />
      </div>
      <div className=" overflow-x-scroll">
        <Table columns={tableCourse} dataSource={dataCourse} />
      </div>
    </div>
  );
}
