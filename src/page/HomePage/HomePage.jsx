import { Button, message } from "antd";
import React, { useEffect, useState } from "react";
import { userService } from "../../services/userService";
export default function HomePage() {
  const [courseData, setCourseData] = useState([]);
  useEffect(() => {
    userService
      .getListCourse()
      .then((res) => {
        setCourseData(res.data);
      })
      .catch((err) => {});
  }, []);
  return (
    <div className="">
      <img
        src="https://www.udemy.com/staticx/udemy/images/v7/logo-udemy.svg"
        alt=""
        className=" w-52 h-40 mx-auto"
      />
      <div className=" px-10 py-5 text-xl">
        <h1>
          Tổng số lượng khóa học trên Udemy : {courseData.length} khóa học
        </h1>
        <h1>
          Tổng số lượng học viên :{" "}
          {courseData.reduce((total, student) => {
            return total + student.soLuongHocVien;
          }, 0)}{" "}
          người
        </h1>
        <h1>
          Tổng số lượt xem :{" "}
          {courseData.reduce((total, student) => {
            return total + student.luotXem;
          }, 0)}{" "}
          lượt xem
        </h1>
      </div>
      <div className=" grid grid-cols-5 gap-5">
        {courseData.map((item) => {
          return (
            <div className=" my-10 border-2 px-3 py-3">
              <img src={item.hinhAnh} className="w-full h-32 mb-5" alt="" />
              <h6>Tên khóa học :{item.tenKhoaHoc}</h6>
              <h6>Số lượt xem : {item.luotXem}</h6>
              <h6>Ngày tạo : {item.ngayTao}</h6>
              <h6>Số lượng học viên : {item.soLuongHocVien}</h6>
              <h6>Người tạo : {item.nguoiTao.hoTen}</h6>
              <h6>Danh mục : {item.danhMucKhoaHoc.tenDanhMucKhoaHoc}</h6>
            </div>
          );
        })}
      </div>
    </div>
  );
}
// {
//     "maKhoaHoc": "haha",
//     "biDanh": "loi-500",
//     "tenKhoaHoc": "Mongo DB",
//     "moTa": "",
//     "luotXem": 500,
//     "hinhAnh": "https://images.viblo.asia/286c9a3f-6a38-4113-addd-7b6e43b537bf.jpg",
//     "maNhom": "GP01",
//     "ngayTao": "10/10/2020",
//     "soLuongHocVien": 100,
//     "nguoiTao": {
//         "taiKhoan": "123",
//         "hoTen": "taikhoangiaovu",
//         "maLoaiNguoiDung": "GV",
//         "tenLoaiNguoiDung": "Giáo vụ"
//     },
//     "danhMucKhoaHoc": {
//         "maDanhMucKhoahoc": "BackEnd",
//         "tenDanhMucKhoaHoc": "Lập trình Backend"
//     }
// }
