import { Button, Form, Input, message } from "antd";
import { Link, useNavigate } from "react-router-dom";
import React from "react";
import { userService } from "../../services/userService";
import { localStorageService } from "../../services/localStorageService";
export default function LoginPage() {
  const navigate = useNavigate();
  const onFinish = (values) => {
    userService
      .postLogin(values)
      .then((res) => {
        console.log(res.data);
        localStorageService.setUserInfo(res.data);
        if (res.data.maLoaiNguoiDung == "GV") {
          message.success("Đăng nhập thành công !");
          setTimeout(() => {
            let curent = window.location.pathname;
            if (curent === "/") {
              window.location.reload();
            } else {
              navigate("/");
            }
          }, 1000);
        } else {
          message.error("Không đủ quyền truy cập");
        }
      })
      .catch((err) => {
        message.error("Sai tài khoản hoặc mật khẩu");
      });
  };
  const onFinishFailed = (errorInfo) => {};
  return (
    <div className="flex justify-center items-center w-full  h-full min-h-screen p-2">
      <div className="w-80 border px-5 py-10 rounded-lg shadow-md">
        <h2 className="text-3xl text-slate-600 mb-5">Đăng nhập</h2>
        <Form
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          name="basic"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Tài khoản"
            name="taikhoan"
            rules={[
              {
                required: true,
                message: "Vui lòng điền tài khoản!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Mật khẩu"
            name="matkhau"
            rules={[
              {
                required: true,
                message: "Vui lòng điền mật khẩu!",
              },
            ]}
          >
            <Input.Password />
          </Form.Item>
          <div className="mx-auto mb-5">
            <Button type="primary" htmlType="submit">
              Đăng nhập
            </Button>
          </div>
        </Form>
      </div>
    </div>
  );
}
