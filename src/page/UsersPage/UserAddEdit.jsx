import { Button, Form, Input, message, Select } from "antd";
import React, { useEffect, useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { userService } from "../../services/userService";
const { Option } = Select;

export default function UserAddEdit({ type }) {
  const [typePage, setTypePage] = useState({
    title: null,
    button: null,
    user: null,
  });
  const [typePerson, setTypePerson] = useState([]);
  const { username } = useParams();
  const navigate = useNavigate();
  const onFinish = (values) => {
    let data = { ...values, maNhom: "GP01" };
    switch (type) {
      case "edit":
        userService
          .putUpdateUser(data)
          .then((res) => {
            message.success("Cập nhật thông tin thành công");
            setTimeout(() => {
              navigate("/danh-sach-hoc-vien");
            }, 2000);
          })
          .catch((err) => {
            message.error(err.response.data);
          });
        break;
      case "add":
        userService
          .postAddUser(data)
          .then((res) => {
            message.success("Đăng kí tài khoản thành công");
            setTimeout(() => {
              navigate("/danh-sach-hoc-vien");
            }, 2000);
          })
          .catch((err) => {
            message.error(err.response.data);
          });
    }
  };

  useEffect(() => {
    userService
      .getTypePerson()
      .then((res) => {
        setTypePerson(res.data);
      })
      .catch((err) => {
        message.error(err.response.data);
      });
    switch (type) {
      case "add":
        setTypePage({
          title: "Thêm người dùng",
          button: "Thêm",
          user: null,
        });
        break;
      case "edit":
        userService
          .postSearchUser(username)
          .then((res) => {
            let i = res.data.findIndex((e) => e.taiKhoan === username);
            setTypePage({
              title: "Chỉnh sửa người dùng",
              button: "Cập nhật",
              user: res.data[i],
            });
          })
          .catch((err) => {});
        break;
    }
  }, []);

  return (
    typePage.title !== null && (
      <div className="p-5">
        <h2>{typePage.title}</h2>
        <div>
          <Form
            name="user"
            onFinish={onFinish}
            autoComplete="off"
            layout="vertical"
            initialValues={{
              taiKhoan: typePage.user?.taiKhoan,
              matKhau: typePage.user?.matKhau,
              hoTen: typePage.user?.hoTen,
              email: typePage.user?.email,
              soDt: typePage.user?.soDt,
              maLoaiNguoiDung: typePage.user?.maLoaiNguoiDung,
            }}
          >
            <div className="grid lg:grid-cols-2 lg:gap-5">
              <div>
                <Form.Item
                  label="Tài khoản"
                  name="taiKhoan"
                  rules={[
                    { required: true, message: "Please input your username!" },
                  ]}
                >
                  <Input disabled={type === "edit" ? true : false} />
                </Form.Item>

                <Form.Item
                  label="Mật khẩu"
                  name="matKhau"
                  rules={[
                    { required: true, message: "Please input your password!" },
                  ]}
                >
                  <Input.Password />
                </Form.Item>
                <Form.Item
                  label="Họ tên"
                  name="hoTen"
                  rules={[
                    { required: true, message: "Please input your name!" },
                  ]}
                >
                  <Input />
                </Form.Item>
              </div>
              <div>
                <Form.Item
                  label="Email"
                  name="email"
                  rules={[
                    { required: true, message: "Please input your username!" },
                  ]}
                >
                  <Input />
                </Form.Item>

                <Form.Item
                  label="Số điện thoại"
                  name="soDt"
                  rules={[
                    { required: true, message: "Please input your password!" },
                  ]}
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  label="Loại người dùng"
                  name="maLoaiNguoiDung"
                  rules={[
                    { required: true, message: "Please input your name!" },
                  ]}
                >
                  <Select defaultValue={typePage.user?.maLoaiNguoiDung}>
                    {typePerson.map((e, i) => (
                      <Option key={i} value={e.maLoaiNguoiDung}>
                        {e.tenLoaiNguoiDung}
                      </Option>
                    ))}
                  </Select>
                </Form.Item>
              </div>
            </div>
            <div className="flex justify-between">
              <Link to="/danh-sach-hoc-vien">{"<< Quay lại"} </Link>
              <Button type="primary" htmlType="submit">
                {typePage.button}
              </Button>
            </div>
          </Form>
        </div>
      </div>
    )
  );
}
