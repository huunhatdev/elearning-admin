import { message, Table } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { userChange } from "../../../redux/Slices/changeSlice";
import { userService } from "../../../services/userService";
import { tableWaitConfirm } from "../../../utils/contentTable/tableWaitConfirm";

export default function ModalConfirmed({
  username,
  handleRender,
  handleOnchange,
}) {
  const [data, setData] = useState([]);
  useEffect(() => {
    userService
      .getCourceConfirmed(username)
      .then((res) => {
        let temp = res.data.map((e, i) => {
          return {
            ...e,
            i: i + 1,
            actions: {
              onCancel: () => {
                userService
                  .postCancelCourse({ ...e, taiKhoan: username })
                  .then((res) => {
                    message.success(res.data);
                    handleOnchange();
                  })
                  .catch((err) => {
                    console.log(err.response.data);
                  });
              },
            },
          };
        });
        console.log(temp);
        setData(temp);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [handleRender]);

  return (
    data.length > 0 && (
      <div className="p-2">
        <h2 className="py-2">Khoá học chờ xác nhận</h2>
        <Table size="small" dataSource={data} columns={tableWaitConfirm} />
      </div>
    )
  );
}
