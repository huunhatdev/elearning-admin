import { Button, Form, message, Select } from "antd";
import React, { useCallback, useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { userChange } from "../../../redux/Slices/changeSlice";
import { userService } from "../../../services/userService";
const { Option } = Select;

export default function ModalCourse({ username, handleOnchange }) {
  const [courseList, setCourseList] = useState([]);

  const handlRegister = (data) => {
    userService
      .postRegisterCourse({ ...data, taiKhoan: username })
      .then((res) => {
        message.success(res.data);
        handleOnchange();
      })
      .catch((err) => {
        message.error(err.response.data);
      });
  };

  useEffect(() => {
    userService
      .getListCourse()
      .then((res) => {
        setCourseList(res.data);
      })
      .catch((err) => {});
  }, []);
  return (
    <div className="p-2 border-b border-b-slate-200">
      <Form name="registerCourse" onFinish={handlRegister} autoComplete="off">
        <div className="flex">
          <div className="w-10/12">
            <Form.Item
              label="Khoá học"
              name="maKhoaHoc"
              rules={[{ required: true, message: "Please chose your course!" }]}
            >
              <Select
                style={{ width: "100%" }}
                showSearch
                placeholder="Select a course"
                optionFilterProp="children"
                filterOption={(input, option) =>
                  option.children.toLowerCase().includes(input.toLowerCase())
                }
              >
                {courseList.map((e, i) => (
                  <Option key={i} value={e.maKhoaHoc}>
                    {e.tenKhoaHoc}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </div>
          <div className="w-2/12 text-right">
            <Button type="primary" htmlType="submit">
              Ghi danh
            </Button>
          </div>
        </div>
      </Form>
    </div>
  );
}
