import { Button, Modal } from "antd";
import React, { useCallback, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { userChange } from "../../../redux/Slices/changeSlice";
import ModalConfirmed from "./ModalConfirmed";
import ModalCourse from "./ModalCourse";
import ModalWaitConfirm from "./ModalWaitConfirm";
export default function ModalRegister({ handleVisible, openModal, taiKhoan }) {
  const handleCancel = () => {
    handleVisible(false);
  };
  let handleRender = useSelector((state) => state.changeSlice.user);
  const dispatch = useDispatch();
  const handleOnchange = useCallback(() => {
    dispatch(userChange());
  });

  useEffect(() => {
    if (openModal) {
      handleOnchange();
    }
  }, [openModal]);

  return (
    <>
      <Modal
        title="Ghi danh khoá học"
        visible={openModal}
        onCancel={handleCancel}
        width={1000}
        footer={[
          <Button key="back" onClick={handleCancel} danger>
            Đóng
          </Button>,
        ]}
      >
        <ModalCourse username={taiKhoan} />
        <ModalWaitConfirm
          username={taiKhoan}
          handleRender={handleRender}
          handleOnchange={handleOnchange}
        />
        <ModalConfirmed
          username={taiKhoan}
          handleRender={handleRender}
          handleOnchange={handleOnchange}
        />
      </Modal>
    </>
  );
}
