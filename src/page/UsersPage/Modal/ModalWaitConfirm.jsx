import { message, Table } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { userChange } from "../../../redux/Slices/changeSlice";
import { userService } from "../../../services/userService";
import { tableWaitConfirm } from "../../../utils/contentTable/tableWaitConfirm";

export default function ModalWaitConfirm({
  username,
  handleRender,
  handleOnchange,
}) {
  const [data, setData] = useState([]);
  useEffect(() => {
    userService
      .getCourceWaitConfirm(username)
      .then((res) => {
        let temp = res.data.map((e, i) => {
          return {
            ...e,
            i: i + 1,
            actions: {
              onConfirm: () => {
                userService
                  .postConfirmCourse({ ...e, taiKhoan: username })
                  .then((res) => {
                    message.success(res.data);
                    handleOnchange();
                  })
                  .catch((err) => {});
              },
              onCancel: () => {
                userService
                  .postCancelCourse({ ...e, taiKhoan: username })
                  .then((res) => {
                    message.success(res.data);
                    handleOnchange();
                  })
                  .catch((err) => {});
              },
            },
          };
        });

        setData(temp);
      })
      .catch((err) => {});
  }, [handleRender]);

  return (
    data.length > 0 && (
      <div className="p-2">
        <h2 className="py-2">Khoá học chờ xác nhận</h2>
        <Table size="small" dataSource={data} columns={tableWaitConfirm} />
      </div>
    )
  );
}
