import { Button, message, Table } from "antd";
import React, { useCallback, useEffect, useState } from "react";
import { AudioOutlined } from "@ant-design/icons";
import { Input } from "antd";
import { tableUser } from "../../utils/contentTable/tableUser";
import { userService } from "../../services/userService";
import { Link, useNavigate } from "react-router-dom";
import ModalRegister from "./Modal/ModalRegister";

const { Search } = Input;

export default function UsersPage() {
  const [dataUser, setDateUser] = useState([]);
  const [openModal, setOpenModal] = useState(false);
  const [userModal, setUserModal] = useState(null);
  const navigate = useNavigate();
  const handleVisible = useCallback((e) => {
    setOpenModal(e);
  });
  const fetch = (tuKhoa) => {
    userService
      .postSearchUser(tuKhoa)
      .then((res) => {
        let dataClone = res.data.map((e, i) => {
          return {
            ...e,
            i: i + 1,
            actions: {
              onRegister: () => {
                handleVisible(true);
                setUserModal(e.taiKhoan);
              },
              onEdit: () => {
                navigate(`chinh-sua/${e.taiKhoan}`);
              },
              onDelete: () => {
                userService
                  .delUser(e.taiKhoan)
                  .then((res) => {
                    console.log(res);
                    message.success(res.data);
                    fetch();
                  })
                  .catch((err) => {
                    console.log(err);
                    message.error(err.response.data);
                  });
              },
            },
          };
        });
        setDateUser(dataClone);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  useEffect(() => {
    fetch(null);
  }, []);

  const onSearch = (value) => fetch(value);
  const onChange = (e) => {
    if (e.target.value === "") {
      fetch(null);
    }
  };
  return (
    <div className="p-5 space-y-5">
      {
        <ModalRegister
          handleVisible={handleVisible}
          openModal={openModal}
          taiKhoan={userModal}
        />
      }
      <div className="text-right  pb-0">
        <Link to="them-moi">
          <Button type="primary"> + Thêm người dùng</Button>
        </Link>
      </div>
      <div className="">
        <Search
          placeholder="Nhập tên hoặc tài khoản người dùng"
          onSearch={onSearch}
          onChange={(e) => onChange(e)}
          enterButton
        />
      </div>
      <div className=" overflow-x-scroll">
        <Table columns={tableUser} dataSource={dataUser} />
      </div>
    </div>
  );
}
