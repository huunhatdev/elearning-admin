import { Breadcrumb, Layout } from "antd";
import { Content } from "antd/lib/layout/layout";
import React from "react";
import FooterLayout from "../components/Layout/FooterLayout";
import HeaderLayout from "../components/Layout/HeaderLayout";
import SideMenu from "../components/Layout/SideMenu";
import LoginPage from "../page/LoginPage/LoginPage";
import { localStorageService } from "../services/localStorageService";
let isLogin = localStorageService.getUserInfo() !== null;

export default function LayoutTheme({ component, breadcrumb }) {
  return isLogin ? (
    <Layout
      style={{
        minHeight: "100vh",
      }}
    >
      <SideMenu />
      <Layout className="site-layout">
        <HeaderLayout />
        <Content className="p-5">
          <Breadcrumb style={{ marginLeft: "2rem" }}>
            {breadcrumb.map((bread, i) => {
              return <Breadcrumb.Item key={i}>{bread}</Breadcrumb.Item>;
            })}
          </Breadcrumb>
          <div className="site-layout-background bg-white h-full mt-3">
            {component}
          </div>
        </Content>
        <FooterLayout />
      </Layout>
    </Layout>
  ) : (
    <LoginPage />
  );
}
