import logo from "./logo.svg";
import "./App.css";
import "antd/dist/antd.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { userRouter } from "./routes/userRouter";
import LayoutTheme from "./HOC/LayoutTheme";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          {userRouter.map((e, i) => {
            return (
              <Route key={i} path={e.path}>
                <Route
                  index
                  element={
                    <LayoutTheme
                      component={e.component}
                      breadcrumb={[e.title]}
                    />
                  }
                />
                {e.childs.length > 0 &&
                  e.childs.map((child, index) => {
                    return (
                      <Route
                        key={index}
                        path={child.path}
                        element={
                          <LayoutTheme
                            component={child.component}
                            breadcrumb={[e.title, child.title]}
                          />
                        }
                      />
                    );
                  })}
              </Route>
            );
          })}
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
