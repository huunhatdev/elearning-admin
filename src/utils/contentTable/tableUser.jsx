import {
  DeleteOutlined,
  EditOutlined,
  FileDoneOutlined,
} from "@ant-design/icons";
import { render } from "@testing-library/react";
import { Button, Tag, Tooltip } from "antd";
import { Link } from "react-router-dom";

export const tableUser = [
  {
    title: "#",
    dataIndex: "i",
    key: "i",
  },
  {
    title: "Tài khoản",
    dataIndex: "taiKhoan",
    key: "taiKhoan",
  },
  {
    title: "Họ tên",
    dataIndex: "hoTen",
    key: "hoTen",
  },
  {
    title: "Email",
    key: "email",
    dataIndex: "email",
  },
  {
    title: "Số ĐT",
    dataIndex: "soDt",
    key: "soDt",
  },
  {
    title: "Loại ND",
    key: "maLoaiNguoiDung",
    dataIndex: "maLoaiNguoiDung",
    render: (loaiND) => {
      let color = "";
      let text = "";
      switch (loaiND) {
        case "HV":
          color = "green";
          text = "Học Viên";
          break;
        case "GV":
          color = "blue";
          text = "Giáo vụ";
          break;
      }
      return (
        <Tag color={color} key={loaiND}>
          {text}
        </Tag>
      );
    },
  },
  {
    title: "Tuỳ chọn",
    key: "actions",
    dataIndex: "actions",
    render: (actions) => {
      return (
        <div className="space-x-1">
          <Tooltip placement="top" title={"Ghi danh"}>
            <Button type="outline" onClick={() => actions.onRegister()}>
              <FileDoneOutlined />
            </Button>
          </Tooltip>
          <Tooltip placement="top" title={"Chỉnh sửa"}>
            <Button type="outline" onClick={() => actions.onEdit()}>
              <EditOutlined />
            </Button>
          </Tooltip>
          <Tooltip placement="top" title={"Xoá"}>
            <Button type="outline" danger onClick={() => actions.onDelete()}>
              <DeleteOutlined />
            </Button>
          </Tooltip>
        </div>
      );
    },
  },
];
