import { Button } from "antd";

export const tableUserWaitConfirm = [
  {
    title: "STT",
    dataIndex: "i",
    key: "i",
  },
  {
    title: "Tên học viên",
    dataIndex: "hoTen",
    key: "hoTen",
  },
  {
    title: "Chờ xác nhận",
    dataIndex: "actions",
    key: "actions",
    render: (actions, record) => {
      return (
        <div className="space-x-2">
          {actions.onConfirm ? (
            <Button type="default" size="small" onClick={actions.onConfirm}>
              Xác thực
            </Button>
          ) : null}

          <Button type="default" size="small" danger onClick={actions.onCancel}>
            Huỷ
          </Button>
        </div>
      );
    },
  },
];
