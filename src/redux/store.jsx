import { configureStore } from "@reduxjs/toolkit";
import changeSlice from "./Slices/changeSlice";
import userSlice from "./Slices/userSlice";

export const store = configureStore({
  reducer: {
    userSlice,
    changeSlice,
  },
});
