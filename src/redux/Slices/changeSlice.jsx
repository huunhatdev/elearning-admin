import { createSlice } from "@reduxjs/toolkit";
import { v4 as uuidv4 } from "uuid";

const initialState = {
  user: null,
  course: null,
};

const changeSlice = createSlice({
  name: "creat_slice",
  initialState,
  reducers: {
    userChange: (state) => {
      state.user = uuidv4();
    },
    courseChange: (state) => {
      state.course = uuidv4();
    },
  },
});

export const { userChange, courseChange } = changeSlice.actions;
export default changeSlice.reducer;
