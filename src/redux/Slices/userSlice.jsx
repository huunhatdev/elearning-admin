import { createSlice } from "@reduxjs/toolkit";
import { localStorageService } from "../../services/localStorageService";

const initialState = {
  user: localStorageService.getUserInfo(),
};

const userSlice = createSlice({
  name: "user_slice",
  initialState,
  reducers: {
    demo: (state, { type, payload }) => {
      state.user = payload;
    },
  },
});

export const { demo } = userSlice.actions;
export default userSlice.reducer;
