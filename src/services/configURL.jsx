import axios from "axios";

export const BASE_URL = "https://elearningnew.cybersoft.edu.vn";

export const httpService = axios.create({
  baseURL: BASE_URL,
  headers: {
    TokenCybersoft:
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCBTw6FuZyAwMyIsIkhldEhhblN0cmluZyI6IjEyLzEyLzIwMjIiLCJIZXRIYW5UaW1lIjoiMTY3MDgwMzIwMDAwMCIsIm5iZiI6MTY0NzUzNjQwMCwiZXhwIjoxNjcwOTUwODAwfQ.p9HfB3wfHiMPOhk-B2tIt1JOp-IxfMXGRoFv610OjtY",
  },
});

httpService.interceptors.request.use(
  function (config) {
    httpService.defaults.headers.common["Authorization"] =
      "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoibG9uZ2RheSIsImh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwOC8wNi9pZGVudGl0eS9jbGFpbXMvcm9sZSI6IkdWIiwibmJmIjoxNjU3NTU4NjkzLCJleHAiOjE2NTc1NjIyOTN9.WJvFkGcSnQJLmxBRRpTUxSQDjEMwv8oZnJR7OnR32uM";

    return config;
  },
  function (err) {
    return Promise.reject(err);
  }
);
httpService.interceptors.response.use(
  function (config) {
    return config;
  },
  function (err) {
    // console.log(err);
    // let status = err.response.status;
    // switch (status) {
    //   case 400:
    //   case 401:
    //   case 403:
    //     window.location.href = "/login";
    // }
    return Promise.reject(err);
  }
);
