import { httpService } from "./configURL";

export const userService = {
  getDataUser: (maNhom = "GP01") => {
    return httpService.get(
      `/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=${maNhom}`
    );
  },
  delUser: (taiKhoan) => {
    return httpService.delete(
      `/api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${taiKhoan}`
    );
  },
  postSearchUser: (tuKhoa = "") => {
    let param = tuKhoa ? `?tuKhoa=${tuKhoa}` : "";
    return httpService.get(`/api/QuanLyNguoiDung/TimKiemNguoiDung${param}`);
  },
  getTypePerson: () => {
    return httpService.get(`/api/QuanLyNguoiDung/LayDanhSachLoaiNguoiDung`);
  },
  putUpdateUser: (data) => {
    return httpService.put(
      `/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung`,
      data
    );
  },
  postAddUser: (data) => {
    return httpService.post(`/api/QuanLyNguoiDung/ThemNguoiDung`, data);
  },

  //Khoá học
  getCourceWaitConfirm: (username) => {
    let data = { taiKhoan: username };
    return httpService.post(
      `/api/QuanLyNguoiDung/LayDanhSachKhoaHocChoXetDuyet`,
      data
    );
  },
  getCourceConfirmed: (username) => {
    let data = { taiKhoan: username };
    return httpService.post(
      `/api/QuanLyNguoiDung/LayDanhSachKhoaHocDaXetDuyet`,
      data
    );
  },
  postRegisterCourse: (data) => {
    return httpService.post(`/api/QuanLyKhoaHoc/DangKyKhoaHoc`, data);
  },
  postConfirmCourse: (data) => {
    return httpService.post(`/api/QuanLyKhoaHoc/GhiDanhKhoaHoc`, data);
  },
  postCancelCourse: (data) => {
    return httpService.post(`/api/QuanLyKhoaHoc/HuyGhiDanh`, data);
  },
  getListCourse: (maNhom = "GP01") => {
    return httpService.get(
      `/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?MaNhom=${maNhom}`
    );
  },
  delCourse: (maKhoaHoc) => {
    return httpService.delete(
      `/api/QuanLyKhoaHoc/XoaKhoaHoc?MaKhoaHoc=${maKhoaHoc}`
    );
  },
  getUserAttendedCourse: (maKhoaHoc) => {
    let data = { maKhoaHoc };
    return httpService.post(
      `/api/QuanLyNguoiDung/LayDanhSachHocVienKhoaHoc`,
      data
    );
  },
  getUserWaitConfirm: (maKhoaHoc) => {
    let data = { maKhoaHoc };
    return httpService.post(
      `/api/QuanLyNguoiDung/LayDanhSachHocVienChoXetDuyet`,
      data
    );
  },
  getUserUnregister: (maKhoaHoc) => {
    let data = { maKhoaHoc };
    return httpService.post(
      `/api/QuanLyNguoiDung/LayDanhSachNguoiDungChuaGhiDanh`,
      data
    );
  },
  getInfoCourse: (maKhoaHoc) => {
    return httpService.get(
      `/api/QuanLyKhoaHoc/LayThongTinKhoaHoc?maKhoaHoc=${maKhoaHoc}`
    );
  },
  getCategoryCourse: () => {
    return httpService.get(`/api/QuanLyKhoaHoc/LayDanhMucKhoaHoc`);
  },
  postUpdateCourse: (form_data) => {
    return httpService.post(
      `/api/QuanLyKhoaHoc/CapNhatKhoaHocUpload`,
      form_data
    );
  },
  postAddCourse: (form_data) => {
    return httpService.post(
      `/api/QuanLyKhoaHoc/ThemKhoaHocUploadHinh`,
      form_data
    );
  },
  postLogin: (data) => {
    return httpService.post(`/api/QuanLyNguoiDung/DangNhap`, data);
  },
  putUpdate: (data) => {
    return httpService.put(
      `/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung`,
      data
    );
  },
};
