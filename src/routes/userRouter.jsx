import { HomeOutlined, ReadOutlined, UserOutlined } from "@ant-design/icons";
import HomePage from "../page/HomePage/HomePage";
import LessonAddEdit from "../page/LessonsPage/LessonAddEdit";
import LessonsPage from "../page/LessonsPage/LessonsPage";
import LoginPage from "../page/LoginPage/LoginPage";
import UserAddEdit from "../page/UsersPage/UserAddEdit";
import UsersPage from "../page/UsersPage/UsersPage";

export const userRouter = [
  {
    path: "/",
    component: <HomePage />,
    isLayout: true,
    icon: <HomeOutlined />,
    title: "Trang chủ",
    childs: [],
  },
  {
    path: "/login",
    component: <LoginPage />,
    isLayout: false,
    icon: <HomeOutlined />,
    title: "Trang đăng ký",
    childs: [],
  },
  {
    path: "/danh-sach-khoa-hoc",
    component: <LessonsPage />,
    isLayout: true,
    icon: <ReadOutlined />,
    title: "Khoá học",
    childs: [
      {
        path: "chinh-sua/:course",
        component: <LessonAddEdit type={"edit"} />,
        title: "Chỉnh sửa",
      },
      {
        path: "them-moi",
        component: <LessonAddEdit type={"add"} />,
        title: "Thêm mới",
      },
    ],
  },
  {
    path: "/danh-sach-hoc-vien",
    component: <UsersPage />,
    isLayout: true,
    icon: <UserOutlined />,
    title: "Học viên",
    childs: [
      {
        path: "chinh-sua/:username",
        component: <UserAddEdit type={"edit"} />,
        title: "Chỉnh sửa",
      },
      {
        path: "them-moi",
        component: <UserAddEdit type={"add"} />,
        title: "Thêm mới",
      },
    ],
  },
];
